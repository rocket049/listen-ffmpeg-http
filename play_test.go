package main

import (
	"bytes"
	"io"
	"os"
	"testing"
)

func TestPlay(t *testing.T) {
	fp, err := os.Open("out.wav")
	if err != nil {
		t.Fatal(err)
	}
	defer fp.Close()
	const length = 84
	var header [length]byte
	n, err := io.ReadFull(fp, header[:])
	if err != nil {
		t.Fatal(err)
	}
	if n != length {
		t.Fatalf("length < %v", length)
	}
	samplesPerSec, channels, blockAlign := getWavInfo(header[:])
	pos := bytes.Index(header[:], []byte{'d', 'a', 't', 'a'})
	t.Logf("samplesPerSec=%v\nchannels=%v\nblockAlign=%v\nEND=%v", samplesPerSec, channels, blockAlign, pos)
	ch := make(chan []byte, 1)
	defer close(ch)
	go play(ch, samplesPerSec, channels, blockAlign/channels)
	buf := make([]byte, 1024)
	var start bool
	for {
		n, _ = fp.Read(buf)
		if n > 0 {
			if !start {
				pos := bytes.Index(header[:], []byte{'d', 'a', 't', 'a'})
				if pos > 0 {
					ch <- buf[pos+8 : n]
					start = true
				}

			} else {
				ch <- buf[:n]
			}

		} else {
			break
		}
	}
}
